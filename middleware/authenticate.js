export default function({ store, route, error }) {
  if (process.client) {
    console.log(store.state.isSignedIn)
    if (!store.state.isSignedIn) {
      const secret = window.prompt('パスワードを入力してください')
      if (secret === 'shinkawa2020') {
        window.alert('認証成功')
        store.dispatch('signIn')
      } else {
        window.alert('パスワードが違います！')
      }
    }
  }
}
